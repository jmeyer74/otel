#!/usr/bin/env python3
#pip install opentelemetry-exporter-otlp-proto-grpc

#from opentelemetry import metrics
#from opentelemetry.sdk.metrics import MeterProvider
#from opentelemetry.sdk.metrics.export import PeriodicExportingMetricReader, ConsoleMetricExporter
#from opentelemetry.sdk.resources import SERVICE_NAME, Resource
#
## Service name is required for most backends,
## and although it's not necessary for console export,
## it's good to set service name anyways.
#resource = Resource(attributes={
#    SERVICE_NAME: "your-service-name"
#})
#
#reader = PeriodicExportingMetricReader(ConsoleMetricExporter())
#provider = MeterProvider(resource=resource, metric_readers=[reader])
#metrics.set_meter_provider(provider)
#

from opentelemetry import metrics
from opentelemetry.exporter.prometheus import PrometheusMetricsReader
from opentelemetry.sdk.metrics import Counter, MeterProvider
from opentelemetry.sdk.metrics.export.controller import PushController
from prometheus_client import start_http_server

# Start Prometheus client
start_http_server(port=8000, addr="localhost")

# Meter is responsible for creating and recording metrics
metrics.set_meter_provider(MeterProvider())
meter = metrics.meter()
# exporter to export metrics to Prometheus
prefix = "MyAppPrefix"
exporter = PrometheusMetricsExporter(prefix)
# controller collects metrics created from meter and exports it via the
# exporter every interval
controller = PushController(meter, exporter, 5)

counter = meter.create_metric(
    "requests",
    "number of requests",
    "requests",
    int,
    Counter,
    ("environment",),
)

# Labelsets are used to identify key-values that are associated with a specific
# metric that you want to record. These are useful for pre-aggregation and can
# be used to store custom dimensions pertaining to a metric
label_set = meter.get_label_set({"environment": "staging"})

counter.add(25, label_set)
input("Press any key to exit...")
