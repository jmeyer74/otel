#!/bin/bash

openssl req -x509 -nodes -newkey ec -pkeyopt ec_paramgen_curve:prime256v1 -keyout key.pem -out cert.pem -sha256 -days 1094 -subj '/CN=MeterReaderHanClient' -addext "certificatePolicies = critical,1.3.6.1.4.1.40732.2.2" -addext "keyUsage = critical,digitalSignature"

openssl x509 -noout -fingerprint -SHA256 -inform pem -in cert.pem | sed -e 's/://g' -e 's/SHA256 Fingerprint=//g' | cut -c1-40
